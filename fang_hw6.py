print """
ASTR344 
Assignment #6 
Rui Fang

Part 1: Rootfinding via the Newton-Raphson Method

Take the same functions that you found the roots for HW5 and find the roots via the NR method.
   f(x) = sinx
   g(x) = x^3 - x - 2
   y(x) = -6 + x + x^2

"""

import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from datetime import timedelta

# Define functions that we want to find roots of
def f(x):
   return np.sin(x)
def g(x):
   return (x**3) - x - 2
def y(x):
   return -6 + x + (x**2)

# Define derivatives of the functions
def d_f(x):
   return np.cos(x)
def d_g(x):
   return 3 * x**2 - 1
def d_y(x):
   return 2 * x + 1


# NR(x, func) returns x - func(x)/func'(x) 
def NR(x, func):
   if func == f:
      return x - f(x)/d_f(x)
   elif func == g:
      return x - g(x)/d_g(x)
   elif func == y:
      return x - y(x)/d_y(x)
   elif func == p:
      return x - p(x)/d_p(x)
   else: 
      return None

# Main function 
def findroot(func, x_initial, tolerance = 10**(-15)):
   while abs(func(x_initial)) >= tolerance:
      x_initial = NR(x_initial, func)
   return x_initial

# The bisection method function copied from hw5
def bisect(func, x1, x2, max_iterations = 40, tolerance = 0.0001):

   if func(x1) * func(x2) > 0: # Return error message if function at the bounds have the same sign.
      return "Function at the bounds are both possitive/negative. Change the bounds and try again."
   
   elif func(x1) * func(x2) == 0: # Check if any of the bounds is a root. 
      if func(x1) == 0 and func(x2) != 0: 
         return x1
      elif func(x2) == 0 and func(x1) != 0: 
         return x2
      else:
         return str(x1) + ' and ' + str(x2)

   else: # When func(x1) * func(x2) < 0, start bisecting. 
      xmid = (x1 + x2) / 2.0
      iteration = 0
      while abs(func(xmid)) >= tolerance:
         if func(x1) * func(xmid) > 0:
            x1 = xmid
         else:
            x2 = xmid
         xmid = (x1 + x2) / 2.0
         iteration = iteration + 1
         assert iteration <= max_iterations, "Reached maximum number of iteration. Try increase maximum to allow for more."
   
      return xmid


# Time algorithm
t1 = datetime. now()
findroot(f, 3.)
t2 = datetime. now()
findroot(g, 1.5)
t3 = datetime. now()
findroot(y, 1.)
t4 = datetime. now()
bisect(f, 2, 4)
t5 = datetime. now()
bisect(g, 1, 2)
t6 = datetime. now()
bisect(y, 0, 5)
t7 = datetime. now()


# Present results
print "\nRoot for f(x) from 2,4:"
print "  NR method:", findroot(f, 3.), "(takes time", t2 - t1,")"
print "  Bisection method:", bisect(f, 2, 4), "(takes time", t5 - t4,")"

print "\nRoot for g(x) from 1,2:"
print "  NR method:", findroot(g, 1.5), "(takes time", t3 - t2,")"
print "  Bisection method:", bisect(g, 1, 2), "(takes time", t6 - t5,")"


print "\nRoot for y(x) from 0,5:"
print "  NR method:", findroot(y, 1.), "(takes time", t4 - t3,")"
print "  Bisection method:", bisect(y, 0, 5), "(takes time", t7 - t6,")"







print"""


Part 2: Finding the Temperature of a Black Body

You observe a galaxy with intensity 1.25e-12 at 870 micron in cgs units. What temperature does
this correspond to?

Planck function: B(T) = C1 / (exp(C2/T) - 1)
                   where C1 = 2 * h * nu^3 / c^2
                         C2 = h * nu / k

                     
"""

h = 6.6260755 * 10**(-27)
c = 2.99792458 * 10**10
k = 1.380658 * 10**(-16) 
lmd = 870 * 10**(-4) # wavelentgh: 870 micron
nu = c / lmd
C1 = 2 * h * nu**3 / c**2
C2 = h * nu / k

def B(T): # Planck function
   return C1 / (np.exp(C2/T) - 1)

def p(T): # Since we want T for B(T) = 1.25*10^(-12), we need to find root of B(T) - 1.25*10^(-12)
   return B(T) - 1.25 * 10**(-12)

def d_p(T):
   return C1 * C2 * np.exp(C2/T) / ((np.exp(C2/T) - 1)**2 * T**2)

print "The temperature is", findroot(p, 300.), "K."

