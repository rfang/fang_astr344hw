print"""
ASTR344 
Assignment #7 Monte Carlo
Rui Fang

Part 2: The Birthday Problem
"""

import numpy as np


number_of_trials = 1000

def check_same_birthday(birthdays_list): # check if there exists same pair of birthdays in a birthday list
	result = False
	for each_birthday in birthdays_list:
		if birthdays_list.count(each_birthday) > 1:
			result = True
	return result


number_of_people_list = [] 

for n in range(number_of_trials):

	birthdays_list = [] # Say, there is no people in the room at the beginning.

	while check_same_birthday(birthdays_list) == False: # Keep adding in new people/birthday until two of them have the same birthday
		new_birthday = int(round(365 * np.random.random()))
		birthdays_list.append(new_birthday)

	number_of_people = len(birthdays_list) # This is the number of people needed to first find two people having the same birthday.
	number_of_people_list.append(number_of_people) 

# After run this process for "number_of_trials" times, the median of the number of people recorded is what we need. 
print """The smallest number of people in a room for the probability to be 
greater than 0.5 that two people in the group have the same birthday is"""
print np.median(number_of_people_list)

