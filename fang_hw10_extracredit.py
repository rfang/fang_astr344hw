print"""
ASTR344 
Assignment #10 Curve Fit - extracredit
Rui Fang

Gaussian fit: 
	f(x) = a * exp(-(x-b)^2/(2*c^2))
	a: the height of the curve's peak 
	b: the position of the center of the peak 
	c: the Gaussian RMS width

	FWHM = 2.354 * c
	Area under the curve = a * c * sqrt(2*pi)

"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# import data
hw10a = np.loadtxt('hw10a.dat')
xa = hw10a[:,0]
ya = hw10a[:,1]

hw10b = np.loadtxt('hw10b.dat')
xb = hw10b[:,0]
yb = hw10b[:,1]

# curve functions
def gaussian(x, a, b, c):
	return a * np.exp(- (x-b)**2/(2 * c**2)) 

def doublegaussian(x, a1, b1, c1, a2, b2, c2):
	return gaussian(x, a1, b1, c1) + gaussian(x, a2, b2, c2)

# fit functions
def gaussian_fit(x, y, p0): # p0 is the initial guess of coefficients
	popt, pcov = curve_fit(gaussian, x, y, p0 = p0)
	a, b, c = popt[0], popt[1], popt[2]
	FWHM = 2.354 * popt[2]
	area = popt[0] * popt[2] * np.sqrt(2 * np.pi)
	
	# print fit values
	print "(Gaussian fit)"
	print "a =", a, "  b =", b, "  c =", c
	print "FWHM = ", FWHM
	print "Area under the curve = ", area

	# plot spectrum and fit 
	plt.plot(x, gaussian(x, *popt), 'r', linewidth = 3.0, label = 'Gaussian fit')


def doublegaussian_fit(x, y, p0):
	popt, pcov = curve_fit(doublegaussian, x, y, p0 = p0)
	a1, b1, c1, a2, b2, c2 = popt[0], popt[1], popt[2], popt[3], popt[4], popt[5]
	
	# print fit values
	print "\n(Double-Gaussian fit)"
	print "a1 =", a1, "  b1 =", b1, "  c1 =", c1
	print "a2 =", a2, "  b2 =", b2, "  c2 =", c2
	
	# Plot
	plt.plot(x, doublegaussian(x, *popt), 'g', linewidth = 3.0, label = 'Double-Gaussian fit')
	plt.plot(x, gaussian(x, popt[0], popt[1], popt[2]), 'y', label = 'Gaussian 1 in Double')
	plt.plot(x, gaussian(x, popt[3], popt[4], popt[5]), 'y', label = 'Gaussian 2 in Double')



# smooth data
def boxcar(l, size): # size of the box can only be odd numbers
	l_copy = np.copy(l)
	leftmost = (size-1)/2
	rightmost = len(l) - (size-1)/2
	for i in range(leftmost, rightmost):
		left = i - (size-1)/2
		right = i + (size-1)/2
		boxcar = l[ left:right ]
		l_copy[i] = np.average(boxcar)
	return l_copy




# present results
print """
====================================================
Figure 1: for hw10a.dat,
"""
plt.figure(1)
plt.plot(xa, ya, 'b.', label = 'Data')
gaussian_fit(xa, ya, p0 = [0.02, 50, 50])
plt.xlabel('velocity (km/s)')
plt.ylabel('temperature')
plt.title('CO emission lines (hw10a.dat)')
plt.legend()


print """
====================================================
Figure 2: for hw10b.dat, non-smoothed spectrum, 
"""
plt.figure(2)
plt.plot(xb, yb, 'b.', label = 'Data')
gaussian_fit(xb, yb, p0 = [0.02, 50, 50])
doublegaussian_fit(xb, yb, p0 = [0.01, -40, 25, 0.01, 40, 25])
plt.xlabel('velocity (km/s)')
plt.ylabel('temperature')
plt.title('CO emission lines (hw10b.dat non-smoothed)')
plt.legend()

print """
====================================================
Figure 3: for hw10b.dat, smoothed spectrum, boxcar_size = 7,
"""
yb_smooth = boxcar(yb, 7)
plt.figure(3)
plt.plot(xb, yb_smooth, 'b.', label = 'Data')
gaussian_fit(xb, yb, p0 = [0.02, 50, 50])
doublegaussian_fit(xb, yb_smooth, p0 = [0.01, -40, 25, 0.01, 40, 25])
plt.xlabel('velocity (km/s)')
plt.ylabel('temperature')
plt.title('CO emission lines (hw10b.dat smoothed)')
plt.legend()

print """
====================================================
Conclusion: With appropriate initial guess, a double Gaussian better fits the data for hw10b
  			than a single Gaussian. Smoothing the spectrum for hw10b would make the double 
  			Gaussian shape more obvious. 

"""
plt.show()