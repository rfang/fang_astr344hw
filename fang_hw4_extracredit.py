print """
ASTR344 
Assignment #4 Integration (extracredit)
Rui Fang

Integrate the function f(x) = x^3 + 2x^2 - 4 over the range [-1,1], using 
 1) piecewise linear method
 2) a trapozoidal integrator
 3) Simpson's rule
 4) Gaussian Quadrature


"""
import numpy as np


step = 0.02
x = np.arange(-1.00, 1.001, step)
# Generate an array of x values from -1 to 1 with spacing = step

def f(x):
	return x**3 + 2 * x**2 - 4
# f(x) = x^3 + 2x^2 - 4


P = 0 # Integral using piecewise linear method 
T = 0 # Integral using a trapozoidal integrator
M = 0 # Integral using mid-point rule

for i in range(0, len(x)-1):
	P = P + f(x[i]) * step
	T = T + step * (f(x[i])+f(x[i+1]))/2
	M = M + step * f((x[i]+x[i+1])/2)

S = (2*M + T)/3 # Integral using Simpson's rule: S = (2M+T)/3
G = f(-1/np.sqrt(3))+f(1/np.sqrt(3)) # Integral using Gaussian Quadrature

ExactValue = -20./3.


print "First, generate an array of x values from -1 to 1. Set step = 0.02. "
print "The exact value for the integral is -20/3, calculated by hand."
print "\nUsing piecewise linear method, I = (x[i+1]-x[i]) * f(x[i]): "
print "  Integral =", P, "  Error =", P - ExactValue
print "\nUsing a trapozoidal integrator, T = (x[i+1]-x[i]) * (f(x[i])+f(x[i+1]))/2: "
print "  Integral =", T, "  Error =", T - ExactValue
print "\nUsing Simpson's rule, S = (2M+T)/3, where M = (x[i+1]-x[i]) * f((x[i]+x[i+1])/2): "
print "  Integral =", S, "  Error =", S - ExactValue
print "\nUsing Gaussian Quadrature, integral over [-1,1] = f(-1/sqrt(3)) + f(1/sqrt(3)): "
print "  Integral =", G, "  Error =", G - ExactValue

