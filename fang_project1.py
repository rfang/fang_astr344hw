"""
ASTR344 
Project #1 Polio Mary
Rui Fang

"""

import numpy as np
import random
import matplotlib.pyplot as plt
import copy
from scipy.spatial import KDTree


def run(N, M, V, D, P=0.9, Time=0, step=0.75):
	# N: room size in meter
	# M: total number of people
	# V: number of people vaccinated
	# D: distance tolerance
	# Time: initial time
	# step: speed of moving straight
	# P: probability of getting infected when closer than D

	class person:
		def __init__(self, step=step):
			# set initial position
			self.x = np.random.random()*N 
			self.y = np.random.random()*N 
			# set initial velocity
			theta = np.random.random()*2*np.pi 
			self.vx = np.cos(theta)*step
			self.vy = np.sin(theta)*step
			# set state (infected? vaccinated?)
			self.isInfected = False
			self.isVaccinated = False

		def coordinate(self):
			return [self.x, self.y]
			
		def movestraight(self):
			# move in a straight line determined by initial velocity 
			self.x += self.vx 
			self.y += self.vy	
			# reflect when crossing boundaries		
			if self.x < 0:
				self.x *= -1
				self.vx *= -1
			if self.y < 0:
				self.y *= -1
				self.vy *= -1
			if self.x > N:
				self.x = 2*N - self.x
				self.vx *= -1
			if self.y > N:
				self.y = 2*N - self.y
				self.vy *= -1

		def moverandom(self):
			# move a random distance between -1.5~1.5m in both x and y directions
			self.x = abs(self.x + (np.random.random()*3-1.5)) 
			self.y = abs(self.y + (np.random.random()*3-1.5))
			if self.x > N:
					self.x = 2*N - self.x
			if self.y > N:
					self.y = 2*N - self.y

		def movewithbuffer(self, other, buffer = 1):
			self.x = abs(self.x + (np.random.random()*3-1.5)) 
			self.y = abs(self.y + (np.random.random()*3-1.5))
			if self.x > N:
					self.x = 2*N - self.x
			if self.y > N:
					self.y = 2*N - self.y
			dist = np.sqrt((self.x-other.x)**2+(self.y-other.y)**2)
			while dist <= buffer:
				self.move(self, other)



	# create a group of M people
	group = []
	for i in range(M): group.append(person())

	# infect one person in the group
	group[0].isInfected = True 

	# vaccinate persons in the rest of the group 
	for i in random.sample(range(1, M), V):
		group[i].isVaccinated = True


	inf_vs_t = []

	while not all((person.isInfected or person.isVaccinated) for person in group):
		infected = []
		all_coord = []
		
		for person in group:
			# get all coordinates
			p = person.coordinate()
			all_coord.append(p)
			# get infected people 
			if person.isInfected:
				infected.append(person)
				
		inf_vs_t.append([Time, len(infected)])

		# infect people
		T = KDTree(np.asarray(all_coord))
		for person in infected:
			# find neighbors within distance tolerance D
			ind = T.query_ball_point(person.coordinate(), D) 
			for i in ind:
				if not group[i].isVaccinated and not group[i].isInfected: # check if infectible
					group[i].isInfected = (np.random.random() < P)
				else: pass
		
		# move people
		for person in group:
			person.movestraight()

		# move person(alternate: with buffer)
		#for i in range(M):
		#	for k in range(M):
		#		if k != i: group[i].movewithbuffer(group[k])

		Time += 1
	
	return Time, inf_vs_t

# func run ends








# plot number of people infected vs. time (for one run)
def plot_inf_vs_t():
	inf_vs_t = run(N = 10, M = 30, V = 0, D = 1.5)[1]
	plt.plot(np.asarray(inf_vs_t)[:,0], np.asarray(inf_vs_t)[:,1], 'ro')
	plt.xlabel('time')
	plt.ylabel('number of people infected')
	plt.title('number of people infected vs. time (for one run)')
	plt.show()

plot_inf_vs_t()








# plot number of people vaccinated vs. time needed to infect other people
def plot_t_vs_V(Ntrials = 5, M = 30):
	# Ntrials: number of times to run with the same conditions to 
	#          get a statistically meaningful value for Time
	t_vs_V = []
	for V in range(M):
		timelist = []
		for i in range(Ntrials):
			timelist.append(run(N = 20, M = M, V = V, D = 4)[0])
		T_avg = np.mean(timelist)
		t_vs_V.append([T_avg, V])

	print t_vs_V
	plt.plot(np.asarray(t_vs_V)[:,1], np.asarray(t_vs_V)[:,0], 'ro')
	plt.xlabel('number of people vaccinated')
	plt.ylabel('time needed to infect other people')
	plt.title('time needed to infect other people vs. number of people vaccinated')
	plt.ylim(0)
	plt.show()

#plot_t_vs_V()












# copy and adjust the func run to show coordinate plots
def runcopy(N, M, V, D, P=0.9, Time=0, step = 1.5):
	# N: room size in meter
	# M: total number of people
	# Time: initial time
	# V: number of people vaccinated
	# D: distance tolerance

	class person:
		def __init__(self, step=step):
			# set initial position
			self.x = np.random.random()*N 
			self.y = np.random.random()*N 
			# set initial velocity
			theta = np.random.random()*2*np.pi 
			self.vx = np.cos(theta)*step
			self.vy = np.sin(theta)*step
			# set state (infected? vaccinated?)
			self.isInfected = False
			self.isVaccinated = False

		def coordinate(self):
			return [self.x, self.y]

		def movestraight(self):
			self.x += self.vx 
			self.y += self.vy	
			# correct for boundaries		
			if self.x < 0:
				self.x *= -1
				self.vx *= -1
			if self.y < 0:
				self.y *= -1
				self.vy *= -1
			if self.x > N:
				self.x = 2*N - self.x
				self.vx *= -1
			if self.y > N:
				self.y = 2*N - self.y
				self.vy *= -1

		def move(self):
			# a person moves a random distance between -1.5~1.5m in both x and y directions
			self.x = abs(self.x + (np.random.random()*3-1.5)) 
			self.y = abs(self.y + (np.random.random()*3-1.5))
			if self.x > N:
					self.x = 2*N - self.x
			if self.y > N:
					self.y = 2*N - self.y

		def movewithbuffer(self, other, buffer = 1):
			# a person moves a random distance between -1.5~1.5m in both x and y directions
			self.x = abs(self.x + (np.random.random()*3-1.5)) 
			self.y = abs(self.y + (np.random.random()*3-1.5))
			if self.x > N:
					self.x = 2*N - self.x
			if self.y > N:
					self.y = 2*N - self.y

			dist = np.sqrt((self.x-other.x)**2+(self.y-other.y)**2)
			while dist <= buffer:
				self.move(self, other)



	# create a group of M people
	group = []
	for i in range(M): group.append(person())

	# infect one person in the group
	group[0].isInfected = True 

	# vaccinate persons in the rest of the group 
	for i in random.sample(range(1, M), V):
		group[i].isVaccinated = True

	while not all((person.isInfected or person.isVaccinated) for person in group):
		infected = []
		all_coord = []
		infected_coord = []
		infectible_coord = []
		vaccinated_coord = []

		for person in group:
			# get coordinate
			p = person.coordinate()
			
			# record coordinates for all, infected, infectible and vaccinated
			all_coord.append(p)
			if person.isInfected:
				infected.append(person)
				infected_coord.append(p)
			elif person.isVaccinated:
				vaccinated_coord.append(p)
			else:
				infectible_coord.append(p)

		# infect 
		T = KDTree(np.asarray(all_coord))
		for person in infected:
			# find neighbors within safe distance
			ind = T.query_ball_point(person.coordinate(), D) 
			for i in ind:
				if not group[i].isVaccinated and not group[i].isInfected: # check if infectible
					group[i].isInfected = (np.random.random() < P)
				else: pass
		
		# move person
		for person in group:
			person.movestraight()

		# move person(alternate: with buffer)
		#for i in range(M):
		#	for k in range(M):
		#		if k != i: group[i].movewithbuffer(group[k])
			
		print Time
		Time = Time+1
		
		
		plt.plot(np.asarray(infected_coord)[:,0], np.asarray(infected_coord)[:,1], 'ro', label = Time)
		plt.plot(np.asarray(infectible_coord)[:,0], np.asarray(infectible_coord)[:,1], 'bo')
		#plt.plot(np.asarray(vaccinated_coord)[:,0], np.asarray(vaccinated_coord)[:,1], 'yo')
		plt.xlabel('x')
		plt.ylabel('y')
		plt.show()
		
# func runcopy ends



#runcopy(20,50,0,2)




