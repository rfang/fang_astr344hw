"""
ASTR344 
Assignment #3 Differentiation 
Rui Fang

Use the following formula to get differentials:

f'(x_i) = [h1/(h2*(h1+h2))] * f(x_i+1) 
        - [(h1-h2)/(h2*h1)] * f(x_i) 
        - [h2/(h1*(h1+h2))] * f(x_i-1),

where h1 = x_i - x_i-1
      h2 = x_i+1 - x_i 

"""


from numpy import *
import matplotlib.pyplot as plt


Observed_data = loadtxt('ncounts_850.dat') 
Observed_x = Observed_data[:,0]  # First column: Log of luminosity 
Observed_y = Observed_data[:,1]  # Second column: Log of dN/dL

Model_data = loadtxt('model_smg.dat')
L = Model_data[:,0]  # First column: Luminosity
N = Model_data[:,1]  # Second column: Number of galaxies above that luminosity (N>L)



h1 = zeros(len(L))
h2 = zeros(len(L))
for i in range(1,len(L)):
	h1[i] = L[i]-L[i-1]
for i in range(0,len(L)-1):
	h2[i] = L[i+1]-L[i]


A = h1/(h2*(h1+h2))
B = (h1-h2)/(h2*h1)
C = h2/(h1*(h1+h2))

differential = zeros(len(L)) # dN/dL for model data
for i in range(1,len(L)-1):
	differential[i] = A[i]*N[i+1] - B[i]*N[i] - C[i]*N[i-1]

Log_L = log10(L)
Log_dif = log10(-differential)


plt.plot(Observed_x, Observed_y, 'ro', label = "Observed data")
plt.plot(Log_L, Log_dif, 'bs', label = "Model data")
plt.xlabel('Logarithm of L')
plt.ylabel('Logarithm of dN/dL')
plt.legend()
plt.show()

