print """
ASTR344 
Assignment #5 Rootfinding
Rui Fang

Write a bisection algorithm that can take an arbitrary function, and return the roots.
Find roots for the following functions: 
   f(x) = sinx
   g(x) = x^3 - x - 2
   y(x) = -6 + x + x^2

"""

import numpy as np
import matplotlib.pyplot as plt

# Define functions that we want to find roots of
def f(x):
   return np.sin(x)

def g(x):
   return (x**3) - x - 2

def y(x):
   return -6 + x + (x**2)

print"""
The bisect function takes five parameters:
   func - an arbitrary function 
   x1 - lower bound
   x2 - upper bound
   max_iterations - maximum number of iterations of bisection
   tolerance - number ~ 0 
Set by default, max_iterations = 40 and tolerance = 0.0001. 

"""
def bisect(func, x1, x2, max_iterations = 40, tolerance = 0.0001):

   if func(x1) * func(x2) > 0: # Return error message if function at the bounds have the same sign.
      return "Function at the bounds are both possitive/negative. Change the bounds and try again."
   
   elif func(x1) * func(x2) == 0: # Check if any of the bounds is a root. 
      if func(x1) == 0 and func(x2) != 0: 
         return x1
      elif func(x2) == 0 and func(x1) != 0: 
         return x2
      else:
         return str(x1) + ' and ' + str(x2)

   else: # When func(x1) * func(x2) < 0, start bisecting. 
      xmid = (x1 + x2) / 2.0
      iteration = 0
      while abs(func(xmid)) >= tolerance:
         if func(x1) * func(xmid) > 0:
            x1 = xmid
         else:
            x2 = xmid
         xmid = (x1 + x2) / 2.0
         iteration = iteration + 1
         assert iteration <= max_iterations, "Reached maximum number of iteration. Try increase maximum to allow for more."
   
      return xmid



# Present results:
print "Suggested by the plots,"
print "\nf(x) has a root between 2 and 4: using bisection method, the result is"
print bisect(f, 2, 4)
print "\ng(x) has a root between 1 and 2: using bisection method, the result is"
print bisect(g, 1, 2)
print "\ny(x) has a root between 1 and 3 and another root between -4 and -2: using bisection method, the result is"
print bisect(y, 1, 3), bisect(y, -4, -2)


# Plot functions: 
x = np.arange(-4, 4, 0.01)

plt.subplot(311)
plt.plot(x, f(x))
plt.axhline(0, color = 'r')
plt.title('f(x)')

plt.subplot(312)
plt.plot(x, g(x))
plt.axhline(0, color = 'r')
plt.title('g(x)')

plt.subplot(313)
plt.plot(x, y(x))
plt.axhline(0, color = 'r')
plt.title('y(x)')

plt.tight_layout()
plt.show()

