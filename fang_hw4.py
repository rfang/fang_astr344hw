"""
ASTR344 
Assignment #4 Integration
Rui Fang

Calculate the angular diameter distance in cosmology, DA, for a range of 
 redshifts from z1 = 0 to z2 = 5.

DA = (1/(1+z2))(Integrate[r, {z, z1, z2}])
 where typically z1 = 0 and r = 3000/sqrt(0.3 * (1+z)^3 + 0.7)

"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import spline

step = 0.2
z = np.arange(0., 5.1, step)
# Create an array of z values from 0. to 5. with spacing = step 

r = np.zeros(len(z))
r = 3000./(np.sqrt(0.3 * (1+z)**3 + 0.7))
# Compute r values corresponding to z 

def Distance(z2):
	integral = 0   
	i = 0
	while z[i] < z2:
		integral = integral + step * (r[i]+r[i+1])/2
		i += 1  
	return integral/(1+z2)
# Function Distance(z2) calculates DA for any given z2

DA = np.zeros(len(z))
for i in range(0, len(z)):
	DA[i] = Distance(z[i])
# Compute DA for z2 ranging from 0. to 5. 


# If using the numpy integrator np.trapz: 
DA_trapz = np.zeros(len(z))
for i in range(0, len(z)): 
	DA_trapz[i] = np.trapz(r[:i+1], z[:i+1])/(1+z[i])


print " For z =", z
print "\n Own numerical integration: \n DA =", DA
print "\n Integration using np.trapz: \n DA =", DA_trapz

plt.plot(z, DA, 'bo', label = "Own numerical integration")
plt.plot(z, DA_trapz, 'r--', label = "Integration using np.trapz")
plt.xlabel('z')
plt.ylabel('DA')
plt.legend()
plt.show()