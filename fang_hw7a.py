print"""
ASTR344 
Assignment #7 Monte Carlo
Rui Fang

Part 1: Calculating pi using Hit or Miss method
"""

import numpy as np


def calculate_pi(sample_size):
	points_within_circle = 0
	
	for i in range(sample_size): 
		# create a random coordinate in a square with the range 0<=x<1 and 0<=y<1
	    point = np.random.random((2,)) 
	    x = point[0]
	    y = point[1]

	    # see if the point is inside the quarter circle with radius 1 centered in origin 
	    if np.sqrt(x**2+y**2) <= 1:
	    	points_within_circle += 1
	    else:
			points_within_circle +=0 

	# Area of the quarter circle = pi * r^2/4 = pi/4
	# Area of the square = r^2 = 1
	# Thus pi = 4 * Area of the quarter circle/Area of the square
	pi = 4. * (points_within_circle/float(sample_size))
	return pi

for n in range(1,6):
	sample_size = 10**n
	print "When Monte Carlo sample size is", sample_size, ", pi =", calculate_pi(sample_size)





    

