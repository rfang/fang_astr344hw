print"""
ASTR344 
Assignment #9 Linear Fit - extracredit
Rui Fang

Linear fit: 
log10(Star Formation Rate) = a1 * log10(CO brightness) + a2 

"""

import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('ks_observables.dat', skiprows=1)
Wco = data[:,0] # CO Brightness
SFR = data[:,1] # starformationrate
N = len(Wco) # total number of elements 

x = np.log10(Wco) # x = log10(CO Brightness)
y = np.log10(SFR) # y = log10(Star Formation Rate) 
sigma_i = np.ones(N) # Set sigma_i = 1 for each data point 

# Define S, Sig_x, Sig_y, Sig_x_sq, Sig_xy
S = np.sum(1/sigma_i**2) 
Sig_x = np.sum(x/sigma_i**2)
Sig_y = np.sum(y/sigma_i**2)
Sig_x_sq = np.sum(x**2/sigma_i**2)
Sig_xy = np.sum(x*y/sigma_i**2)

# Compute a1, a2 and report
a1 = (Sig_y * Sig_x_sq - Sig_x * Sig_xy)/(S * Sig_x_sq - Sig_x**2)
a2 = (S * Sig_xy - Sig_y * Sig_x)/(S * Sig_x_sq - Sig_x**2)

print "Fit values: a1 =", a1, "   a2 =", a2

# Plot
plt.plot(x,y,'bo', label = 'Data')
plt.plot(x,a1+a2*x, label = 'Linear fit')
plt.xlabel('log10(CO Brightness)')
plt.ylabel('log10(Star Formation Rate)')
plt.title('Relation Between Star Formation Rate and Gas in Galaxies')
plt.legend()
plt.show()
