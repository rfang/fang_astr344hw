print """                                                                                               
                                                                                                     
ASTR344                                                                                                 
Assignment #2 Stability                                                                                 
Rui Fang                                                                                                
                                                                                                        
                                                                                                        
In this assignment, we use double precision floating point numbers and single precision 
floating point numbers to compute the power function x^n. We will look at the accuracy 
and stability of the calculation for different base number x, by computing absolue errors
and relative errors. 

Here the power function is obtained by implementing a recurrence relation:                                               
    x_0 = 1,                                                                                               
    x_1 = 1/3,                                                                                             
    x_n+1 = (13/3)x_n - (4/3)x_n-1.      

Absolute error and relative error are defined as: 
    absolute error = e(x) = x - xbar
    relative error = er(x) = (x - xbar)/x
    where x is real number, xbar is floating point number. 

    In our cases, x is double precision floating point number, 
                  xbar is single precision floating point number. 

"""


import numpy as np

a1 = 13./3.
a2 = 4./3.

def power_sp(n,x): # Compute x^n in single precision floating point number   
  if n == 0:
     return np.float32(1.)
  elif n == 1:
     return np.float32(x)
  else:
     return a1 * power_sp(n-1,x) - a2 * power_sp(n-2,x)
                                                                                

def power_dp(n,x): # Compute x^n in double precision floating point number 
  if n == 0:
     return 1.     # In python, the floating point by default uses double precision.   
  elif n == 1:
     return x
  else:
     return a1 * power_dp(n-1,x) - a2 * power_dp(n-2,x)

                                                                              

def e(n,x): # Compute absolute error 
  return power_dp(n,x)-power_sp(n,x) 
def er(n,x): # Compute relative error 
  return e(n,x)/power_dp(n,x)                                                                                 



print "Set x = 1/3 to calculate (1/3)^n."
print "  At n = 5:"
print "     (1/3)^5 in single precision floating point number is ", power_sp(5,1./3.)
print "     (1/3)^5 in double precision floating point number is ", power_dp(5,1./3.)
print "     relative error =", e(5,1./3.)
print "     absolute error =", er(5,1./3.)

print "  At n = 20:"
print "     (1/3)^20 in single precision floating point number is ", power_sp(20,1./3.)
print "     (1/3)^20 in double precision floating point number is ", power_dp(20,1./3.)
print "     relative error =", e(20,1./3.)
print "     absolute error =", er(20,1./3.)



print "\nNow set x = 4 to calculate (4)^n."
print "  At n = 20:"
print "     (4)^20 in single precision floating point number is ", power_sp(20,4.)
print "     (4)^20 in double precision floating point number is ", power_dp(20,4.)
print "     relative error =", e(20,4.)
print "     absolute error =", er(20,4.)

print "\nThis calculation is stable because 4^n is an integer, which doesn't cause rounding errors."
print "Relative error should be used to measure accuracy and stability."
